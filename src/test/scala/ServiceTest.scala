package cv.flamp

import org.scalatest._
import akka.http.scaladsl.model.{HttpEntity, ContentTypes, HttpResponse}
import akka.util.ByteString
import spray.json._
import scala.concurrent.Future
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import scala.concurrent.Await
import scala.concurrent.duration._


case class TestService(resp: String => JsValue) extends Service {
  import scala.concurrent.ExecutionContext.Implicits.global
  override def request(url: String) = {
    val data = ByteString(resp(url).compactPrint)
    Future(HttpResponse(entity = HttpEntity.Strict(ContentTypes.`application/json`, data)))
  }
}

class ServiceSpec extends FlatSpec with JsonProtocol {
  implicit val system = ActorSystem("cv-flamp")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  def runSync[R](f: Future[R]) = Await.result(f, Duration.Inf)

  "getCompany" should "return Option[SearchItem] wrapped in Future" in {
    val item = SearchItem("1", "some name", Some("some address"))
    val resp = SearchResponse(Some(List(item))).toJson
    val company = runSync(TestService(_ => resp).getCompany("-", "-"))
    assert(company == Some(item))
  }

  it should "return None wrapped in Future if there are no companies" in {
    val resp = SearchResponse(None).toJson
    val company = runSync(TestService(_ => resp).getCompany("-", "-"))
    assert(company == None)
  }

  "getRating" should "return Option[FlampResponse] wrapped in Future" in {
    val item = SearchItem("1", "some name", Some("some address"))
    val flampResp = FlampResponse(Some("1.1"))
    val rating = runSync(TestService(_ => flampResp.toJson).getRating(item))
    assert(flampResp == rating)
  }

  "getCompanyRating" should "return Option[ResponseItem] wrapped in Future" in {
    val city = "SomeSity"
    val what = "wut?"
    val item = SearchItem("1", "some name", Some("some address"))
    val searchResp = SearchResponse(Some(List(item))).toJson
    val flampResp = FlampResponse(Some("1.1"))

    val searchUrl = API.makeSearchUrl(what, city)
    val profileUrl = API.makeProfileUrl(item.id)
    def makeResp: String => JsValue = {
      case `searchUrl` => searchResp
      case `profileUrl` => flampResp.toJson
    }
    val resp = runSync(TestService(makeResp).getCompanyRating(what, city))
    assert(resp == Some(ResponseItem(item.name, s"${city}, ${item.address.get}", 1.1)))
  }

  it should "return None wrapped in Future if there are no companies" in {
    val city = "SomeSity"
    val what = "wut?"
    val searchResp = SearchResponse(None).toJson
    val flampResp = FlampResponse(Some("1.1"))

    val searchUrl = API.makeSearchUrl(what, city)
    def makeResp: String => JsValue = {
      case `searchUrl` => searchResp
      case _ => flampResp.toJson
    }
    val resp = runSync(TestService(makeResp).getCompanyRating(what, city))
    assert(resp == None)
  }

}
