package cv.flamp

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.Materializer
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import scala.concurrent.duration._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport

import spray.json._

case class SearchItem(id: String, name: String, address: Option[String])
case class SearchResponse(result: Option[List[SearchItem]])
case class FlampResponse(rating: Option[String])

case class ResponseItem(name: String, address: String, rating: BigDecimal)

trait JsonProtocol extends DefaultJsonProtocol {
  implicit val searchItemFormat = jsonFormat3(SearchItem)
  implicit val searchRespFormat = jsonFormat1(SearchResponse)
  implicit val flampRespFormat = jsonFormat1(FlampResponse)
  implicit val respItemFormat = jsonFormat3(ResponseItem)
}

object API {
  val cities = List(
    "Новосибирск",
    "Омск",
    "Томск",
    "Кемерово",
    "Новокузнецк")
  val url = "http://catalog.api.2gis.ru"
  val key = "ruuxah6217"

  def encode(str: String) = java.net.URLEncoder.encode(str, "utf-8")

  def makeSearchUrl(what: String, city: String) =
    s"$url/search?key=$key&version=1.3&what=${encode(what)}&where=${encode(city)}&sort=rating&pagesize=5"

  def makeProfileUrl(id: String) =
    s"$url/profile?key=$key&version=1.3&id=${id}"
}

trait Service extends JsonProtocol with SprayJsonSupport {
  def request(url: String): Future[HttpResponse]

  def getCompany(what: String, city: String)
      (implicit ec: ExecutionContext, mat: Materializer): Future[Option[SearchItem]] =
    for {
      req <- request(API.makeSearchUrl(what, city))
      sr <- Unmarshal(req.entity).to[SearchResponse]
    } yield sr.result.getOrElse(List()).headOption

  def getRating(company: SearchItem)
      (implicit ec: ExecutionContext, mat: Materializer): Future[FlampResponse] =
    for {
      req <- request(API.makeProfileUrl(company.id))
      fr <- Unmarshal(req.entity).to[FlampResponse]
    } yield fr

  def getCompanyRating(what: String, city: String)
      (implicit ec: ExecutionContext, mat: Materializer): Future[Option[ResponseItem]] =
    getCompany(what, city).flatMap {
      case Some(company) =>
        println(company)
        getRating(company).map { flampRating =>
          Some(ResponseItem(
            company.name,
            company.address.map(a => s"${city}, ${a}").getOrElse(city),
            BigDecimal(flampRating.rating.getOrElse("0"))))
        }
      case None => Future(None)
    }
}


 
object WebServer extends App with Service {
  implicit val system = ActorSystem("cv-flamp")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  val http = Http()

  def request(url: String): Future[HttpResponse] =
      http.singleRequest(HttpRequest(uri = url))

  val route = (path(Segment) & get) { what =>
    complete { 
      Future.sequence(API.cities.map { getCompanyRating(what, _) }).map { items =>
        items.flatten.sortBy(_.rating).reverse
      }
    }
  }
 
  Http().bindAndHandle(route, "localhost", 8080)
}
